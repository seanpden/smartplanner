//
//  EditAlertViewController.swift
//  SmartPlanner
//
//  Created by Student Account  on 3/30/22.
//

import Foundation
import UIKit
import CoreData

class EditAlertViewController: UIViewController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func setup()
    {
        
        alertView.layer.cornerRadius = 8.0
        //set term/def
        termEditText.text = card.term
        definitionEditText.text = card.definition
        //make it so it shows this is editable
        termEditText.becomeFirstResponder()
    }
    
}
