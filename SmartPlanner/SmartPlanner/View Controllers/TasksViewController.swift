//
//  TasksViewController.swift
//  SmartPlanner
//
//  Created by Student Account  on 3/29/22.
//

import Foundation
import CoreData
import UIKit

class TasksViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate{
    
    var tasks: [Tasks] = []
    
    @IBOutlet var BackButton: UIButton!
    @IBOutlet var AddTask: UIButton!
    
    override func viewDidLoad() {
            super.viewDidLoad()
        }
    
    func cntxt() -> NSManagedObjectContext {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        return context
    }
    
    func dataset() -> [Tasks]{
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Tasks")
        var data: [Tasks] = []
        do{
            data = try context.fetch(fetchRequest) as! [Tasks]
        }
        catch{
            print(error)
        }
        tasks = data
        do {
            try context.save()
        }
        catch{
            print(error)
        }
        return tasks
    }
    
func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    <#code#>
}

func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    <#code#>
}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let tks = tasks[indexPath.row]
        let alert = UIAlertController(title: tks.term, message: cds.definition, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Edit", style: .default, handler: {_ in
            self.createCustomAlert(card: self.cards[indexPath.row])
            tableView.reloadData()
        }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        tableView.reloadData()
        self.present(alert, animated: true)
    }
    
    func createCustomAlert(tasks: Tasks)
    {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let alertVC = sb.instantiateViewController(identifier: "EditAlertViewController") as! EditAlertViewController
        alertVC.parentVC = self
        alertVC.tasks = card
        alertVC.modalPresentationStyle = .overCurrentContext
        self.present(alertVC, animated: false, completion: nil)
        tableView.reloadData()    }

}
